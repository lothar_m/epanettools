# epanet tools
 
Python Interface for EPANET2. Requires the epanet programmer's toolkit to function.
Please check https://pypi.python.org/pypi/EPANETTOOLS#downloads for extra information.
